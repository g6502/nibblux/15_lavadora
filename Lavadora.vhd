library ieee;
use ieee.std_logic_1164.all;

entity Lavadora is
	port(
		clk	: in	std_logic;
		Arranque	: in	std_logic;
		Llena	: in	std_logic;
		T_agotado	: in	std_logic;
		Secar	: in	std_logic;
		Val_Agua	: out	std_logic;
		Modo_ag	: out	std_logic;
		Modo_gi	: out	std_logic);
end Lavadora;

architecture behave of Lavadora is

	type Maquina_Lavadora	is (Inactiva, Llenar, Agitar, Girar);

begin

	process(clk)
		
		variable ciclo	: Maquina_Lavadora	:= Inactiva;
		
	begin
		if	rising_edge(clk) then
		
			case ciclo is
				when inactiva	=>
					if Arranque = '1' then
						ciclo := Llenar;
					else
						ciclo := inactiva;
					end if;
				
				when Llenar	=>
					if Llena = '1' then
						ciclo := Agitar;
					else
						ciclo := Llenar;
					end if;
				when Agitar	=>
					if T_agotado = '1' then
						ciclo := Girar;
					else
						ciclo := Agitar;
					end if;
				when Girar	=>
					if Secar = '1' then
						ciclo := Inactiva;
					else
						ciclo := Girar;
					end if;
			end case;
		end if;
		
		case ciclo is
				when inactiva	=>
					Val_Agua <= '0';
					Modo_ag	<= '0';
					Modo_gi	<= '0';
				when Llenar	=>
					Val_Agua <= '1';
					Modo_ag	<= '0';
					Modo_gi	<= '0';
				when Agitar	=>
					Val_Agua <= '0';
					Modo_ag	<= '1';
					Modo_gi	<= '0';
				when Girar	=>
					Val_Agua <= '0';
					Modo_ag	<= '0';
					Modo_gi	<= '1';
			end case;
			
	end process;

end behave;